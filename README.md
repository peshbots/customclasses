# customclasses

A repository for our custom classes. You should be able to clone this repository as a submodule.

This repository's package name is com.gitlab.peshbots, so be sure to put it in `/TeamCode/src/main/java/com/gitlab/peshbots`!

You can view the API documentation for these classes [here](https://peshbots.gitlab.io/customclasses).

Written by Daniel Parks during the 2018-19 Rover Ruckus season.
