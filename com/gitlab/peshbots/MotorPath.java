package com.gitlab.peshbots;
/**
 * Represents a sequence of motor actions
 */
public class MotorPath {
	/**
	 * Represents a single motor action
	 */
	public static class Entry {
		private DcMotorPartAuto motor;
		private int target;
		private double speed;
		private int timeoutMs;
		private boolean absolute;
		private boolean started = false;

		/**
		 * Constructor
		 * @param speed the speed at which to turn the motor
		 * @param motor the motor to turn
		 * @param position how far to turn it/where to turn it
		 * @param timeoutMs how long to wait before giving up
		 * @param absolute whether to interpret position as a relative (false) or absolute (true) measurement
		 */
		public Entry (double speed, DcMotorPartAuto motor, int position, int timeoutMs, boolean absolute) {
			this.speed = speed;
			this.motor = motor;
			this.target = position;
			this.absolute = absolute;
			this.timeoutMs = timeoutMs;
		}

		/**
		 * Returns whether the action is currently running
		 * @return whether the action is currently running
		 */
		public boolean isRunning() {
			if (started) {
				return motor.checkIfAutoRunning();
			}
			return false;
		}

		/**
		 * Begins the action if it has not begun. Otherwise, checks if the motor is finished and stops it if so. This should be run <i>repeatedly</i> while the action is running.
		 * @return whether the motor is still running <i>after</i> the method has performed its task
		 */
		public boolean run() {
			if (! started) { // need to start
				started = true;
				if (absolute) {
					motor.autoRunToPosition(speed, target, timeoutMs);
				}
				else {
					motor.autoRunToPosition(speed, target + motor.getCurrentPosition(), timeoutMs);
				}
				return true;
			}
			return motor.autoRun();
		}

		/**
		 * Starts the action if necessary, then blocks until it is complete
		 */
		public void runUntilComplete() {
			while (run());
		}
	}

	private Entry[][] steps;
	private int state = 0;
	private boolean complete = false;
	private boolean resetWhenComplete = false;

	/**
	 * Constructor
	 * @param steps the steps to take in the motorpath
	 * The steps argument should be a two dimensional array of the following format:
	 * Each row will be executed serially.
	 * Within each row, each action will be executed in parallel. For example, to lower the claw, then lift the claw and retract it at the same time:
	 * <pre>
	 * {@code
	 * MotorPath myMotorPath = new MotorPath(new Entry[][]{
	 *   new Entry[]{new Entry(1.0, clawLifter, -10, 1, false)},
	 *   new Entry[]{new Entry(1.0, clawLifter, 10, 1, false), new Entry(1.0, clawSlider, -20, 1, false)}
	 * });
	 * }
	 * </pre>
	 */
	public MotorPath (Entry[][] steps) {
		this.steps = steps;
	}

	public MotorPath (Entry[][] steps, boolean resetWhenComplete) {
		this.steps = steps;
		this.resetWhenComplete = resetWhenComplete;
	}

	/**
	 * Either runs or starts the action sequence
	 * @return whether the action sequence is running <i>after</i> this method has performed its function
	 */
	public boolean runOrStart() {
		if (! complete) {
			boolean stateFinished = true;
			for (Entry i: steps[state]) {
				if (i.run()) { // this motor hasn't finished running
					stateFinished = false;
				}
			}
			if (stateFinished) {
				if (state + 1 < steps.length) {
					state++;
					runOrStart();
				}
				else {
					complete = true;
					this.reset();
					return false;
				}
			}
			return true;
		}
		return false;
	}

	/**
	 * Returns whether the action sequence is complete
	 * @return whether the action sequence is complete
	 */
	public boolean isComplete() {
		return complete;
	}

	public boolean isRunning() { return state > 0 && ! complete; }

	public void reset() {
		if (! isRunning()) {
			state = 0;
			complete = false;
		}
		else {
			throw new MotorPathInvalidStateException("Tried to reset while running!");
		}
	}

	/**
	 * Starts the action sequence if it is not already running, then blocks until the sequence is complete
	 */
	public void runUntilComplete() {
		while (runOrStart());
	}
}
