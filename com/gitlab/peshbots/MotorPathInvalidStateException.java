package com.gitlab.peshbots;

public class MotorPathInvalidStateException extends RuntimeException {
    MotorPathInvalidStateException(String msg) {
        super(msg);
    }

    MotorPathInvalidStateException(String msg, Throwable cause) {
        super(msg, cause);
    }
}
