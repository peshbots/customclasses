package com.gitlab.peshbots;

import com.qualcomm.robotcore.hardware.CRServo;

public class DoubleCRServo {
    private CRServo s1 = null;
    private CRServo s2 = null;

    public DoubleCRServo(CRServo s1, CRServo s2) {
        this.s1 = s1;
        this.s2 = s2;
    }

    public void setDirection(CRServo.Direction d1, CRServo.Direction d2) {
        s1.setDirection(d1);
        s2.setDirection(d2);
    }

    public CRServo.Direction[] getDirection() {
        return new CRServo.Direction[]{s1.getDirection(), s2.getDirection()};
    }

    public void setPower(double p) {
        s1.setPower(p);
        s2.setPower(p);
    }

    public double[] getPower() {
        return new double[]{s1.getPower(), s2.getPower()};
    }
}
