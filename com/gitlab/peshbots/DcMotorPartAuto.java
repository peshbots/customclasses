package com.gitlab.peshbots;

import com.qualcomm.robotcore.hardware.DcMotorController;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorImpl;
import com.qualcomm.robotcore.hardware.configuration.typecontainers.MotorConfigurationType;
import com.qualcomm.robotcore.util.ElapsedTime;

/** This class represents a DcMotor which can control its own position OR be controlled by any of the standard motor methods.
 */
public class DcMotorPartAuto extends DcMotorImpl implements DcMotor {

    public enum AutoRunMode {
        MANUAL,
        TARGET,
        INDEFINITE
    }

    private AutoRunMode isAutoRunning = AutoRunMode.MANUAL;
    private double autoEndMs = 0;
    private boolean limited = false;
    private boolean reversedGearbox = false;
    private int lowerLimit = 0;
    private int upperLimit = 0;
    private ElapsedTime runtime = new ElapsedTime();

    //------------------------------------------------------------------------------------------------
    // Construction ( I have to do this because there is no default constructor! )
    //------------------------------------------------------------------------------------------------

    /**
     * Constructor
     *
     * @param controller DC motor controller this motor is attached to
     * @param portNumber portNumber position on the controller
     */
    public DcMotorPartAuto(DcMotorController controller, int portNumber) {
        super(controller, portNumber);
    }

    /**
     * Constructor
     *
     * @param controller DC motor controller this motor is attached to
     * @param portNumber portNumber port number on the controller
     * @param direction direction this motor should spin
     */
    public DcMotorPartAuto(DcMotorController controller, int portNumber, Direction direction) {
        super(controller, portNumber, direction);
    }

    /**
     * Constructor
     *
     * @param controller DC motor controller this motor is attached to
     * @param portNumber portNumber port number on the controller
     * @param direction direction this motor should spin
     * @param motorType the type we know this motor to be
     */
    public DcMotorPartAuto(DcMotorController controller, int portNumber, Direction direction, MotorConfigurationType motorType) {
        super(controller, portNumber, direction, motorType);
    }

    /**
     * Constructs a DcMotorPartAuto from a motor object (useful for acting on the results of hardwaremap.get())
     * @param motor the motor to use in construction
     */
    public DcMotorPartAuto(DcMotor motor) {
        super(motor.getController(), motor.getPortNumber(), motor.getDirection(), motor.getMotorType());
    }

    public DcMotorPartAuto(DcMotor motor, boolean reversedGearbox) {
        super(motor.getController(), motor.getPortNumber(), motor.getDirection(), motor.getMotorType());
        this.reversedGearbox = true;
    }

    // other methods -- helpers for part-auto motors

    /**
     * Like setPower, except if the motor is already running to a position, check if it is finished and stop
     *
     * @param power from -1.0 to 1.0
     * @return whether the power was actually set
     */
    public synchronized boolean setPowerOrAutoRun(double power) {
        if (this.isAutoRunning != AutoRunMode.MANUAL) { // if the motor is autorunning
            this.autoRun(); // ignore the input and handle autorun
            return false;
        }
        else {
            if (this.limited) {
                int currentPos = this.getCurrentPosition();
//                int powerMultiplier = reversedGearbox ? -1 : 1;
                if (currentPos > this.upperLimit && power > 0) {
                    this.setPower(0);
                    return false;
                }
                else if (currentPos < this.lowerLimit && power < 0) {
                    this.setPower(0);
                    return false;
                } // ignore the request if the motor is beyond its limits
                else {
                    this.setPower(power);
                    return true;
                }
            }
            this.setPower(power);
            return true;
        }
    }

    /**
     * Check if the motor is finished autorunning, and stop it and turn it off if so
     * @return whether the motor is autorunning AFTER this check
     */
    public synchronized boolean autoRun () {
        if (this.isAutoRunning == AutoRunMode.TARGET) {
            // perform auto-run checks
            if (runtime.milliseconds() > this.autoEndMs || ! this.isBusy()) {
                this.setPower(0);
                this.setMode(RunMode.RUN_USING_ENCODER);
                this.isAutoRunning = AutoRunMode.MANUAL;
                return false;
            }
            return true;
        }
        return false;
        // do not throw an exception if the motor is not running. this is a check
    }

    /**
     * Returns the motor's auto-run state
     * @return the motor's auto-run states
     */
    public AutoRunMode checkAutoRunning() {
        return this.isAutoRunning;
    }

    /**
     * Returns true if the motor is auto-running
     * @return whether the motor is auto-running
     */
    public boolean checkIfAutoRunning() {
        return this.isAutoRunning != AutoRunMode.MANUAL;
    }


    /**
     * Begin an automated motion.
     * This function is named the same as the function that <i>continues</i> the autorun just to irritate you.
     * @param power from -1.0 to 1.0
     * @param revolutions the number of revolutions to turn before stopping
     * @param timeoutMs the number of seconds to wait before giving up in the event that the motor moves slower than expected (i.e. stall)
     */
    public synchronized void autoRun(double power, int revolutions, int timeoutMs) {
        this.isAutoRunning = AutoRunMode.TARGET;
        this.setTargetPosition(this.getCurrentPosition() + revolutions);
        this.setMode(RunMode.RUN_TO_POSITION);
        runtime.reset();
        autoEndMs = runtime.milliseconds() + timeoutMs;
        this.setPower(Math.abs(power));
    }

    public synchronized void autoRunToPosition(double power, int position, int timeoutMs) {
        this.isAutoRunning = AutoRunMode.TARGET;
        this.setTargetPosition(position);
        this.setMode(RunMode.RUN_TO_POSITION);
        runtime.reset();
        autoEndMs = runtime.milliseconds() + timeoutMs;
        if (! reversedGearbox)
            this.setPower(Math.abs(power));
        else
            this.setPower(-Math.abs(power));
    }


    /**
     * Start indefinite autorun (basically, take control from the user)
     * @param power from -1.0 to 1.0
     */
    public synchronized void autoRunIndefinite(double power) {
        this.isAutoRunning = AutoRunMode.INDEFINITE;
        this.setMode(RunMode.RUN_USING_ENCODER);
        this.setPower(power);
    }

    /**
     * Stop autorun (basically, take control of the motor back)
     */
    public synchronized void stopAutoRun() {
        if (this.isAutoRunning != AutoRunMode.MANUAL) {
            this.setMode(RunMode.RUN_USING_ENCODER);
            this.setPower(0);
            this.isAutoRunning = AutoRunMode.MANUAL;
        }
    }

    /**
     * Set the limits on the motor
     * @param lower duh
     * @param upper duh
     */
    public synchronized void setLimits(int lower, int upper) {
        this.lowerLimit = lower;
        this.upperLimit = upper;
        this.limited = true;
    }

    public synchronized void disableLimits() {
        this.lowerLimit = 0;
        this.upperLimit = 0;
        this.limited = false;
    }

    @Override
    protected int adjustPosition(int position) {
        if (getOperationalDirection() == Direction.REVERSE) position = -position;
        if (reversedGearbox) position = -position;
        return position;
    }
}
