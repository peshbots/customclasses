package com.gitlab.peshbots.testing;

import com.gitlab.peshbots.DcMotorPartAuto;
import com.qualcomm.robotcore.hardware.DcMotorController;

public class DcMotorPartAutoTester {
	public static void main(String[] args) {
		DcMotorPartAuto testerMotor = new DcMotorPartAuto(new DcMotorController(), 0);
		testerMotor.autoRun(1.0, 10, 1);
		while (testerMotor.autoRun());
	}
}
