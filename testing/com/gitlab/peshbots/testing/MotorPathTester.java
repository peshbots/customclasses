package com.gitlab.peshbots.testing;

import com.qualcomm.robotcore.hardware.DcMotorController;
import com.gitlab.peshbots.DcMotorPartAuto;
import com.gitlab.peshbots.MotorPath;
import static com.gitlab.peshbots.MotorPath.Entry;

public class MotorPathTester {
	public static void main(String[] args) {
		DcMotorPartAuto shoulder = new DcMotorPartAuto(new DcMotorController(), 0);
		DcMotorPartAuto elbow    = new DcMotorPartAuto(new DcMotorController(), 0);
		MotorPath testPath = new MotorPath(new Entry[][]{
			new Entry[]{new Entry(1.0, shoulder, 10, 1, true)},
			new Entry[]{new Entry(1.0, elbow, 10, 1, true)},
			new Entry[]{new Entry(0.5, shoulder, 5, 1, false), new Entry(1.0, elbow, 10, 1, false)}
		});
		testPath.runUntilComplete();
	}
}
