package com.qualcomm.robotcore.hardware;

import com.qualcomm.robotcore.hardware.configuration.typecontainers.MotorConfigurationType;

public class DcMotorImpl implements DcMotor {

	private static int instanceCount = 0;

	private double power;
	private double position;
	private int target;
	private int name;
	
	public DcMotorImpl(DcMotorController controller, int portNumber) {
		power = 0.0;
		position = 0.0;
		target = 0;
		this.name = instanceCount++;
	}
	public DcMotorImpl(DcMotorController controller, int portNumber, Direction direction) {
		this(controller, portNumber);
	}
	public DcMotorImpl(DcMotorController controller, int portNumber, Direction direction, MotorConfigurationType motorType) {
		this(controller, portNumber);
	}
	
	public void setPower(double power) {
		System.out.println(name + ": power " + power);
		this.power = power;
	}
	
	public int getCurrentPosition() {
		return (int) position;
	}
	
	public boolean isBusy() {
		this.position += this.power;
		System.out.println(name + ": move to " + this.position);
		return this.position < this.target;
	}
	
	public void setMode(RunMode mode) {
		System.out.println(name + ": RunMode " + mode.toString());
	}
	
	public void setTargetPosition(int position) {
		System.out.println(name + ": target " + position);
		this.target = position;
	}

	public DcMotorController getController() {
		return new DcMotorController();
	}

	public int getPortNumber() {
		return 0;
	}

	public Direction getDirection() {
		return Direction.FORWARD;
	}

	public MotorConfigurationType getMotorType() {
		return new MotorConfigurationType();
	}
}
