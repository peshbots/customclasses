package com.qualcomm.robotcore.hardware;

import com.qualcomm.robotcore.hardware.configuration.typecontainers.MotorConfigurationType;

public interface DcMotor {
	public enum RunMode {
		RUN_TO_POSITION,
		RUN_USING_ENCODER
	}

	public enum Direction {
		FORWARD,
		REVERSE
	}

	public DcMotorController getController();
	public int getPortNumber();
	public Direction getDirection();
	public MotorConfigurationType getMotorType();
}
