package testing;

/**
 * Contains all of the dependencies necessary create a bogus DcMotorPartAuto
 */
public class DcMotorFaker {
	public static class DcMotorImpl implements DcMotor {
		public enum RunMode {
			RUN_TO_POSITION,
			RUN_USING_ENCODER
		}
		
		private double power;
		private double position;
		private int target;
		private String name;
		
		public DcMotorImpl(DcMotorController controller, int portNumber, String name) {
			power = 0.0;
			position = 0.0;
			target = 0;
			this.name = name;
		}
		
		public void setPower(double power) {
			System.out.println(name + ": power " + power);
			this.power = power;
		}
		
		public int getCurrentPosition() {
			return (int) position;
		}
		
		public boolean isBusy() {
			this.position += this.power;
			System.out.println(name + ": move to " + this.position);
			return this.position < this.target;
		}
		
		public void setMode(RunMode mode) {
			System.out.println(name + ": RunMode " + mode.toString());
		}
		
		public void setTargetPosition(int position) {
			System.out.println(name + ": target " + position);
			this.target = position;
		}
	}
	
	public static interface DcMotor {}
	
	public static class DcMotorController {}
	
	public static class Direction {}
	
	public static class MotorConfigurationType {}
	
	public static class ElapsedTime {
		public int milliseconds() {
			return 0;
		}
		public void reset() {}
		
	}
}
